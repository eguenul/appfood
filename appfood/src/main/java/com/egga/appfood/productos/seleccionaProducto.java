/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.productos;

import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet(urlPatterns = "/seleccionaProducto", name = "seleccionaProducto")
public class seleccionaProducto extends HttpServlet {
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
    String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
    HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
    ProductoDAO objProductoDAO = new ProductoDAO(objHibernate.getSessionFactory());
    int productoId = Integer.parseInt(request.getParameter("productoId"));
    Producto objProducto = objProductoDAO.obtenerProducto(productoId);
    response.setContentType("application/json;charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.print("{");
    out.print("\"id\": \"" + objProducto.getProductoId() + "\",");
    out.print("\"nombre\": \"" + objProducto.getProductoNom() + "\",");
    out.print("\"precio\": \"" + objProducto.getProductoPrecio() + "\"");
    out.print("}");
    objHibernate.closeSessionFactory();
}
    
}
