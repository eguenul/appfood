package com.egga.appfood.ordenes;

import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet(urlPatterns = "/BuscaOrden", name = "BuscaOrden")
public class BuscaOrden extends HttpServlet {

    
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
try{  
    
int ordenNum = Integer.parseInt(request.getParameter("ordenNum"));
    
String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
OrdenDAO objOrdenDAO = new OrdenDAO(objHibernate.getSessionFactory());
  
Orden objOrden = objOrdenDAO.buscarPorNum(ordenNum);

List<Orden> arrayOrdenList = new ArrayList<>();
arrayOrdenList.add(objOrden);

request.getSession().setAttribute("arrayOrdenList", arrayOrdenList);
 getServletConfig().getServletContext().getRequestDispatcher("/ordenview/listorden.jsp").forward(request,response);
} catch(IOException | NumberFormatException | ServletException e){
   
    PrintWriter out = response.getWriter();
    out.println("<div style='color: red; font-weight: bold;'>La orden no fue encontrada.</div>");
   
}


}



}
