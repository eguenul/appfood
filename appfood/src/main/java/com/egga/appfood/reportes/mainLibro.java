/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.reportes;

import com.egga.appfood.categoria.Categoria;
import com.egga.appfood.categoria.CategoriaDAO;
import com.egga.appfood.include.HibernateUtil;
import com.egga.appfood.productos.Producto;
import com.egga.appfood.productos.ProductoDAO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */@WebServlet(urlPatterns = "/mainLibro", name = "mainLibro")
public class mainLibro extends HttpServlet {
    @Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
      HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
      ProductoDAO objProductoDAO = new ProductoDAO(objHibernate.getSessionFactory());
      List<Producto> listaproducto = objProductoDAO.listarProductosConLimite(); 
      request.getSession().setAttribute("listaproducto", listaproducto);
     
      
    CategoriaDAO objCategoriaDAO = new CategoriaDAO(objHibernate.getSessionFactory());
    List<Categoria> arrayCategoria = objCategoriaDAO.obtenerTodasCategorias();
    request.getSession().setAttribute("arrayCategoria", arrayCategoria);
      
      
      getServletConfig().getServletContext().getRequestDispatcher("/reportview/libro.jsp").forward(request,response);
      objHibernate.closeSessionFactory();
  
    }
    
}
