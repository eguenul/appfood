function agregaDetalle() {
    var tabla = document.getElementById("TablaDetalle").getElementsByTagName('tbody')[0];
    var fila = tabla.insertRow();

    var celda1 = fila.insertCell(0);
    var celda2 = fila.insertCell(1);
    var celda3 = fila.insertCell(2);
    var celda4 = fila.insertCell(3);
    var celda5 = fila.insertCell(4);
    var celda6 = fila.insertCell(5);
    
    // Nueva celda para el botón

    // Crear un botón de eliminación
    var botonEliminar = document.createElement("button");
    botonEliminar.setAttribute("type", "button");
    botonEliminar.innerHTML = "Eliminar";
    var nro_filas = document.getElementById('NRO_FILAS').value;

    botonEliminar.onclick = function () {
         eliminaFila(nro_filas);
    };

    // Agregar el botón a la nueva celda
    var nombre = document.createElement("input");
    var cantidad = document.createElement("input");
    var precio = document.createElement("input");
    var total = document.createElement("input");
    
    
    var productoId = document.createElement("input");
    var observacion = document.createElement("input");


    cantidad.setAttribute('size', '3');
    precio.setAttribute('size', '3');
    total.setAttribute('size', '3');
    
    
    
    nombre.setAttribute('readonly', 'yes');
    cantidad.setAttribute('readonly', 'yes');
    productoId.setAttribute("type", "hidden")
    
    
    celda1.appendChild(nombre);
    celda1.appendChild(productoId);
    celda2.appendChild(cantidad);
    celda3.appendChild(precio);
    celda4.appendChild(total);
    
    celda5.appendChild(observacion);
    celda6.appendChild(botonEliminar);




    // Asignar IDs y nombres
    asignarIDNombre("CANTIDAD", nro_filas, cantidad);
    asignarIDNombre("NOMBRE", nro_filas, nombre);
    asignarIDNombre("FILA", nro_filas, fila);
    asignarIDNombre("BOTON", nro_filas, botonEliminar);
    asignarIDNombre("PRECIO", nro_filas, precio);
    asignarIDNombre("TOTAL", nro_filas, total);
    
    
    asignarIDNombre("observacion", nro_filas, observacion);
    asignarIDNombre("productoId", nro_filas, productoId);

    // Asignar valores
    asignarValor("auxProductoId", productoId);
    asignarValor("producto", nombre);
    asignarValor("cantidad", cantidad);
    asignarValor("observaciones", observacion);

    asignarValor("precio", precio);
    asignarValor("total", total);



    // Limpiar campos
    limpiarCampos("producto", "cantidad", "observaciones", "precio","total");

    // Actualizar el número de filas
    actualizarNumeroFilas();
    calcularTotalOrden();
}

function eliminaFila(idFila) {
    var tabla = document.getElementById("TablaDetalle").getElementsByTagName('tbody')[0];
    var fila = document.getElementById("FILA" + idFila);

    if (fila) {
        tabla.removeChild(fila);
        actualizarNumeroFilas();
    } else {
        console.error("La fila con ID " + idFila + " no existe.");
    }
    calcularTotalOrden();
}

function asignarIDNombre(prefix, nro_filas, element) {
    var id = prefix + nro_filas;
    element.setAttribute('id', id);
    element.setAttribute('name', id);
}

function asignarValor(sourceId, targetElement) {
    var value = document.getElementById(sourceId).value;
    targetElement.value = value;
}

function limpiarCampos() {
    for (var i = 0; i < arguments.length; i++) {
        document.getElementById(arguments[i]).value = '';
    }
}

function actualizarNumeroFilas() {
    var tabla = document.getElementById("TablaDetalle").getElementsByTagName('tbody')[0];
    var nro_filas = tabla.rows.length;
    document.getElementById("NRO_FILAS").value = nro_filas;
}



function calcularTotal() {
        var cantidad = document.getElementById("cantidad").value;
        var precio = document.getElementById("precio").value;
        var total = cantidad * precio;
        // Muestra el total en el campo correspondiente
        document.getElementById("total").value = total;
}



function calcularTotalOrden(){
      var nro_filas = document.getElementById('NRO_FILAS').value;
      var auxTotal = 0;
      for(i=0; i<=nro_filas - 1; i++){
           auxTotal = auxTotal + parseInt(document.getElementById("TOTAL"+i).value); 
      }
      document.getElementById('totalOrden').value = auxTotal;
}



function grabarOrden(){
 
   document.getElementById('OrdenFecha').value = document.getElementById('auxFechaOrden').value;
   document.formComanda.submit();

}

    
    




    