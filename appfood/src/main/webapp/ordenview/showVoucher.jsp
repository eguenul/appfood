<%@page import="com.egga.appfood.fpago.FPago"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Procesar Pago</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <%@include file="../include/nav.jsp" %>

    <div class="container mt-4">
        <h1 class="mb-4">Procesar Pago</h1>

        <div class="mb-3">
            <button class="btn btn-secondary" onclick="goBack()">Volver</button>
        </div>

        <div class="mb-4">
            <embed src="DownloadVoucher" type="application/pdf" width="100%" height="600px" />
        </div>

       
     
       
    </div>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>

    <!-- Agregar enlaces a los archivos JS de Bootstrap y Popper.js (necesario para algunos componentes de Bootstrap) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</body>
</html>

