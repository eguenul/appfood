/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.correlativo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
/**
 *
 * @author esteban
 */public class CorrelativoDAO {
    
    private final SessionFactory sessionFactory;

    public CorrelativoDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public int obtenerUltimoCorrelativo() {
        try (Session sesion = sessionFactory.openSession()) {
            NativeQuery<?> query = sesion.createNativeQuery("SELECT OrdenNum FROM Correlativos");
            Integer resultado = (Integer) query.uniqueResult();
            return (resultado != null) ? resultado : 0;
        }
    }

    public void actualizarCorrelativo() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            try {
                NativeQuery<?> query = session.createNativeQuery("UPDATE Correlativos SET OrdenNum = OrdenNum + 1");
                query.executeUpdate();
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                throw e;
            }
        }
    }
    
    public int obtenerCorrelativoVoucher() {
        try (Session sesion = sessionFactory.openSession()) {
            NativeQuery<?> query = sesion.createNativeQuery("SELECT NumVoucher FROM Correlativos");
            Integer resultado = (Integer) query.uniqueResult();
            return (resultado != null) ? resultado : 0;
        }
    }

     public void actualizarVoucher() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            try {
                NativeQuery<?> query = session.createNativeQuery("UPDATE Correlativos SET NumVoucher = NumVoucher + 1");
                query.executeUpdate();
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                throw e;
            }
        }
    }
    
    
}
