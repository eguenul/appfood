
<%@page import="java.util.List"%>
<%@page import="com.egga.appfood.categoria.Categoria"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Formulario de Producto</title>
    <!-- Agrega los enlaces a los archivos CSS de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@include file="../include/nav.jsp" %>
<div class="container mt-5">
    <h2 class="mb-4">Formulario de Producto</h2>
    
    <% if(session.getAttribute("mensajeOK")=="OK"){ %>
    
     <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">�xito!</h4>
        <p>El registro de Productos ha sido actualizado con �xito.</p>
     </div>
    
    <%
     session.removeAttribute("mensajeOK");
     } %>
    
    
    
    <!-- Formulario -->
    <form method="POST" action="addProducto">
        <div class="mb-3">
            <label for="ProductoId" class="form-label">ID de Producto:</label>
            <input type="text" class="form-control" id="ProductoId" name="ProductoId">
        </div>
        
        <div class="mb-3">
            <label for="ProductoNom" class="form-label">Nombre de Producto:</label>
            <input type="text" class="form-control" id="ProductoNom" name="ProductoNom" required>
        </div>

        <div class="mb-3">
            <label for="CategoriaId" class="form-label">Categor�a:</label>
            <select class="form-select" id="CategoriaId" name="CategoriaId" required>
           <% List<Categoria> listCategoria = (List<Categoria>)request.getSession(true).getAttribute("arrayCategoria");
 %>               
                
            <%   for(Categoria categoria : listCategoria){ %>
             <option value="<% out.print(categoria.getCategoriaId()); %>"><% out.print(categoria.getCategoriaDes()); %></option>
            <% } %>
            </select>
        </div>

        <div class="mb-3">
            <label for="ProductoPrecio" class="form-label">Precio:</label>
            <input  type="number" class="form-control" id="ProductoPrecio" name="ProductoPrecio" required>
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
    <!-- Fin del formulario -->

</div>

 
<!-- Agrega los enlaces a los archivos JS de Bootstrap y Popper.js (necesario para algunos componentes de Bootstrap) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>