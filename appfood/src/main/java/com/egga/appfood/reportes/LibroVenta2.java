/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.reportes;

import com.egga.appfood.include.HibernateUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */




 @WebServlet(urlPatterns = "/LibroVenta2", name = "LibroVenta2")
public class LibroVenta2 extends HttpServlet {
 @Override
public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
    
 String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
   HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
      
    
// Obtener la conexión de la sesión
     try (Session session = objHibernate.getSessionFactory().openSession()) {
         // Obtener la conexión de la sesión
         Connection hibernateConnection = ((SessionImpl) session).connection();
         String fechaInicio = request.getParameter("fechaInicio");
         String fechaFin = request.getParameter("fechaFin");
         String formato = request.getParameter("Formato");
         
         
         
         
         
         
         // Configurar parámetros, en este caso, agregar la conexión de Hibernate
         Map<String, Object> hm = new HashMap<>();
         hm.put("FechaInicio", fechaInicio);
         hm.put("FechaFin", fechaFin);
         

      JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile(getServletContext().getRealPath("/reports/Ventas.jasper"));
      JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, hm, hibernateConnection);
      
      
      switch(formato){
          
          case "PDF":
          
                        OutputStream output = new FileOutputStream(new File("/home/esteban/Venta.pdf")); 
                        JasperExportManager.exportReportToPdfStream(jasperPrint, output);    
                        getServletConfig().getServletContext().getRequestDispatcher("/reportview/showlibro1pdf.jsp").forward(request,response);
                        break;

          case "XLS":
                        JRXlsExporter exporter = new JRXlsExporter();
                        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("/home/esteban/Venta.xls"));
                        SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
                        configuration.setOnePagePerSheet(true);
                        exporter.setConfiguration(configuration);
                        exporter.exportReport();
                         getServletConfig().getServletContext().getRequestDispatcher("/reportview/showlibro1xls.jsp").forward(request,response);
                      
                        break;
            
            
            
      }
        
        
        
// Cerrar la sesión de Hibernate
session.close();
     } catch (JRException ex) {
         Logger.getLogger(LibroVenta.class.getName()).log(Level.SEVERE, null, ex);
     }
}
    
       
}