
package com.egga.appfood.ordenes;

import com.egga.appfood.correlativo.CorrelativoDAO;
import com.egga.appfood.include.HibernateUtil;
import com.egga.appfood.productos.Producto;
import com.egga.appfood.productos.ProductoDAO;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/addOrden", name = "addOrden")
public class addOrden extends HttpServlet{
  
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    try {
        String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
        HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
        OrdenDAO objOrdenDAO = new OrdenDAO(objHibernate.getSessionFactory());
        
        CorrelativoDAO objCorrelativo = new CorrelativoDAO(objHibernate.getSessionFactory());
        
        
        int ordennum = objCorrelativo.obtenerUltimoCorrelativo();
        
        
        
        System.out.print(ordennum);
        Orden objOrden = new Orden();
        
        
        
        String dateString = request.getParameter("OrdenFecha");
        
        // El formato de la cadena
        String pattern = "yyyy-MM-dd";

        // Crear un objeto SimpleDateFormat con el patrón
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        
        Date date = dateFormat.parse(dateString);
        
        objOrden.setOrdenFecha(date);
        objOrden.setOrdenTotal(Integer.parseInt(request.getParameter("totalOrden")));
        
        
        objOrden.setOrdenNum(ordennum);
        
        
        objOrdenDAO.guardarOrden(objOrden);
        int ordenid = objOrdenDAO.getId(ordennum);
       
        
        
        int nrofilas = Integer.parseInt(request.getParameter("NRO_FILAS"));
        System.out.print(nrofilas);
        
        
        for(int i=0;i<=nrofilas-1;i++){
           System.out.print("la fila es"+ i);
            
           DetalleOrdenDAO objDetalleDAO = new DetalleOrdenDAO(objHibernate.getSessionFactory());
           DetalleOrden objDetalleOrden = new DetalleOrden(); 
           
          int productoId = Integer.parseInt(request.getParameter("productoId"+String.valueOf(i)));
           
           ProductoDAO objProductoDAO = new ProductoDAO (objHibernate.getSessionFactory());
           Producto objProducto = objProductoDAO.obtenerProducto(productoId);
           
           objDetalleOrden.setProducto(objProducto);
           objDetalleOrden.setOrdenid(ordenid);
           objDetalleOrden.setCantidad(Integer.parseInt(request.getParameter("CANTIDAD"+String.valueOf(i))));
           objDetalleOrden.setPrecio(Integer.parseInt(request.getParameter("PRECIO"+String.valueOf(i))));
           objDetalleOrden.setTotal(Integer.parseInt(request.getParameter("TOTAL"+String.valueOf(i))));
           objDetalleOrden.setObservacion(request.getParameter("observacion"+String.valueOf(i)));
           
           
           objDetalleDAO.addDetalle(objDetalleOrden);
         
        }
     objCorrelativo.actualizarCorrelativo();    
    request.getSession().setAttribute("ordenNum", ordennum);
    objHibernate.closeSessionFactory();
    response.sendRedirect("PrintOrden");
    
    } catch (ParseException ex) {
        Logger.getLogger(addOrden.class.getName()).log(Level.SEVERE, null, ex);
    }
   

}



    
    
}


