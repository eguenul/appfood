/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.ordenes;

import com.egga.appfood.fpago.FPago;
import java.util.Date;

/**
 *
 * @author esteban
 */
public class Orden {
    
   
    private int ordenId;
    
    
    
    
    
    
    
    private int ordenTotal;
   
    private int ordenNum;
   
   
   

    private Date ordenFecha;
    
    private FPago formapago;
    
    
    
    public Orden(){
        
               
    }
    
    
    
    public int getOrdenId() {
        return ordenId;
    }

    public void setOrdenId(int ordenId) {
        this.ordenId = ordenId;
    }

    public int getOrdenTotal() {
        return ordenTotal;
    }

    public void setOrdenTotal(int ordenTotal) {
        this.ordenTotal = ordenTotal;
    }

    
    
    

    public Date getOrdenFecha() {
        return ordenFecha;
    }

    public void setOrdenFecha(Date ordenFecha) {
        this.ordenFecha = ordenFecha;
    }

    public FPago getFormapago() {
        return formapago;
    }

    public void setFormapago(FPago formapago) {
        this.formapago = formapago;
    }

    public int getOrdenNum() {
        return ordenNum;
    }

    public void setOrdenNum(int ordenNum) {
        this.ordenNum = ordenNum;
    }


    
    
}
