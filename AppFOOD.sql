-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: AppFOOD
-- ------------------------------------------------------
-- Server version	8.0.35-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Categoria`
--

DROP TABLE IF EXISTS `Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Categoria` (
  `CategoriaId` int NOT NULL AUTO_INCREMENT,
  `CategoriaDes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CategoriaId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categoria`
--

LOCK TABLES `Categoria` WRITE;
/*!40000 ALTER TABLE `Categoria` DISABLE KEYS */;
INSERT INTO `Categoria` VALUES (1,'PLATOS PRINCIPALES'),(2,'PLATOS DE ENTRADA'),(3,'POSTRES'),(4,'VINOS Y LICORES'),(5,'BEBIDAS Y JUGOS'),(6,'COMPLETOS');
/*!40000 ALTER TABLE `Categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Correlativos`
--

DROP TABLE IF EXISTS `Correlativos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Correlativos` (
  `OrdenNum` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Correlativos`
--

LOCK TABLES `Correlativos` WRITE;
/*!40000 ALTER TABLE `Correlativos` DISABLE KEYS */;
INSERT INTO `Correlativos` VALUES (165);
/*!40000 ALTER TABLE `Correlativos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DetalleOrden`
--

DROP TABLE IF EXISTS `DetalleOrden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DetalleOrden` (
  `DetalleOrdenId` int NOT NULL AUTO_INCREMENT,
  `OrdenId` int DEFAULT NULL,
  `ProductoId` int DEFAULT NULL,
  `Cantidad` int DEFAULT NULL,
  `Precio` int DEFAULT NULL,
  `Total` int DEFAULT NULL,
  `Observacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`DetalleOrdenId`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DetalleOrden`
--

LOCK TABLES `DetalleOrden` WRITE;
/*!40000 ALTER TABLE `DetalleOrden` DISABLE KEYS */;
INSERT INTO `DetalleOrden` VALUES (158,159,3,1,2000,2000,''),(159,159,4,1,2000,2000,''),(160,160,13,1,3500,3500,''),(161,160,3,1,2000,2000,''),(162,161,3,1,2000,2000,''),(163,162,3,1,2000,2000,''),(164,163,3,1,2000,2000,''),(165,163,13,1,3500,3500,''),(166,164,4,1,2000,2000,''),(167,164,5,1,2000,2000,''),(168,165,6,2,2000,4000,''),(169,165,13,3,3500,10500,''),(170,166,3,1,2000,2000,''),(171,166,7,1,2000,2000,''),(172,167,3,1,2000,2000,''),(173,167,13,1,3500,3500,''),(174,168,3,1,2000,2000,'1'),(175,169,13,1,3500,3500,''),(176,169,3,1,2000,2000,''),(177,170,3,1,2000,2000,''),(178,170,4,1,2000,2000,''),(179,170,13,2,3500,7000,'1 ITALIANO SIN MAYO');
/*!40000 ALTER TABLE `DetalleOrden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FPago`
--

DROP TABLE IF EXISTS `FPago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FPago` (
  `FPagoId` int NOT NULL AUTO_INCREMENT,
  `FPagoDes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`FPagoId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FPago`
--

LOCK TABLES `FPago` WRITE;
/*!40000 ALTER TABLE `FPago` DISABLE KEYS */;
INSERT INTO `FPago` VALUES (1,'EFECTIVO'),(2,'TARJETA DE DEBITO'),(3,'TARJETA DE CREDITO'),(4,'CHEQUE');
/*!40000 ALTER TABLE `FPago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orden`
--

DROP TABLE IF EXISTS `Orden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Orden` (
  `OrdenId` int NOT NULL AUTO_INCREMENT,
  `OrdenNum` int DEFAULT NULL,
  `OrdenFecha` date DEFAULT NULL,
  `OrdenTotal` int DEFAULT NULL,
  `FPagoId` int DEFAULT NULL,
  `OrdenServ` int DEFAULT NULL,
  `OrdenPago` int DEFAULT NULL,
  `NumVoucher` int DEFAULT NULL,
  PRIMARY KEY (`OrdenId`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orden`
--

LOCK TABLES `Orden` WRITE;
/*!40000 ALTER TABLE `Orden` DISABLE KEYS */;
INSERT INTO `Orden` VALUES (159,153,'2023-12-10',4000,NULL,NULL,NULL,NULL),(160,154,'2023-12-10',5500,NULL,NULL,NULL,NULL),(161,155,'2023-12-10',2000,NULL,NULL,NULL,NULL),(162,156,'2023-12-10',2000,NULL,NULL,NULL,NULL),(163,157,'2023-12-10',5500,NULL,NULL,NULL,NULL),(164,158,'2023-12-10',4000,NULL,NULL,NULL,NULL),(165,159,'2023-12-10',14500,NULL,NULL,NULL,NULL),(166,160,'2023-12-10',4000,NULL,NULL,NULL,NULL),(167,161,'2023-12-11',5500,NULL,NULL,NULL,NULL),(168,162,'2023-12-11',2000,NULL,NULL,NULL,NULL),(169,163,'2023-12-11',5500,NULL,NULL,NULL,NULL),(170,164,'2023-12-11',11000,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Orden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Producto`
--

DROP TABLE IF EXISTS `Producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Producto` (
  `ProductoId` int NOT NULL AUTO_INCREMENT,
  `ProductoNom` varchar(45) DEFAULT NULL,
  `ProductoPrecio` varchar(45) DEFAULT NULL,
  `CategoriaId` int DEFAULT NULL,
  PRIMARY KEY (`ProductoId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Producto`
--

LOCK TABLES `Producto` WRITE;
/*!40000 ALTER TABLE `Producto` DISABLE KEYS */;
INSERT INTO `Producto` VALUES (3,'COCA COLA LATA NORMAL','2000',5),(4,'SPRITE LATA','2000',5),(5,'orange crush lata','2000',5),(6,'FANTA NARANJA LATA','2000',5),(7,'PEPSI LATA','2000',5),(8,'SEVEN UP LATA','2000',5),(9,'KEM PIÑA LATA','2000',5),(10,'COCA COLA ZERO LATA','2000',5),(11,'BILZ LATA','2000',5),(12,'PAP LATA','2000',5),(13,'COMPLETO ITALIANO','3500',6);
/*!40000 ALTER TABLE `Producto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-11 16:25:47
