
/* global documento */

function buscaProductoNom(){
    
        var parmNombre = document.getElementById('ProductoNom2').value;
        cargarAjax('buscaProductoNom','ProductoNom='+parmNombre,'tablaProductosBody');
    
}


function filtrarProductos(){
    
    var categoriaId = document.getElementById("selectCategoria").value;
    var var_pagina = document.getElementById("pagina").value;
    
    // Hacer la llamada Ajax
    $.ajax({
        type: 'POST',
        url: 'buscaPorCategoria',
        data: {
            CategoriaId: categoriaId,
            pagina: var_pagina
        },
        success: function (data) {
            // Limpiar la tabla
            $("#tablaProductosBody").empty();

            // Actualizar la tabla con los nuevos datos
            for (var i = 0; i < data.productos.length; i++) {
                var producto = data.productos[i];
                var fila = "<tr><td>" + producto.id + "</td><td>" + producto.nombre + "</td><td>" + producto.precio + "</td>"+ "<td><button type=\"button\" onclick=\"seleccionaProducto('" +producto.id + "')\">Seleccionar</button></td></tr>";
                $("#tablaProductosBody").append(fila);
            }

            
            document.getElementById("paginaActual").innerText = data.pagina;
            document.getElementById("NRO_PAGINAS").value = data.totalPaginas;
            document.getElementById("pagina").value = data.pagina;
           
            
       },
        error: function () {
            alert("Error al cargar productos");
        }
    });
}


function cambiarPagina(accion) {
    var paginaActual = parseInt(document.getElementById("pagina").value);
    var nro_paginas = parseInt(document.getElementById("NRO_PAGINAS").value);

    if (document.getElementById("selectCategoria").value == 0) {
        alert('Debe seleccionar categoría');
    } else {
        switch (accion) {
            case "anterior":
                paginaActual = (paginaActual > 1) ? paginaActual - 1 : nro_paginas;
                break;

            case "siguiente":
                paginaActual = (paginaActual < nro_paginas) ? paginaActual + 1 : 1;
                break;
        }

        document.getElementById("pagina").value = paginaActual;
        filtrarProductos();
    }
}




function seleccionaProducto(productoId){
    
    // Hacer la llamada Ajax
    $.ajax({
        type: 'POST',
        url: 'seleccionaProducto',
        data: {
            productoId: productoId
            
        },
        success: function (data) {
         document.getElementById("producto").value = data.nombre;
         document.getElementById("auxProductoId").value = data.id;
         document.getElementById("precio").value = data.precio;
         
            
            
            cerrarModalProductos();
        
        },
        error: function () {
            alert("Error al cargar productos");
        }
    });
}

function cerrarModalProductos() {
 $("#modalProductos").modal("hide");
  $(".modal-backdrop").remove(); // Elimina la sombra de fondo
}