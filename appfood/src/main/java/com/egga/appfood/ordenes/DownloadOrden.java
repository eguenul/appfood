/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.ordenes;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/DownloadOrden", name = "DownloadOrden")
public class DownloadOrden extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

         File file = new File("/home/esteban/appfood/PDF/orden.pdf");
        response.setHeader("Content-Type", getServletContext().getMimeType(file.getName().trim()));
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "inline; filename=\""+"orden.pdf" + "\"");
        Files.copy(file.toPath(), response.getOutputStream());
        file.delete();
       // Ajusta la ruta al directorio donde se genera el PDF por el servlet PrintOrden
      
}
}
