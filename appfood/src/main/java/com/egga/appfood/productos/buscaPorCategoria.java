/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.productos;

import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/buscaPorCategoria", name = "buscaPorCategoria")
public class buscaPorCategoria extends HttpServlet{
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// Lógica para buscar productos por categoría y enviar la respuesta al cliente    
String categoriaId = request.getParameter("CategoriaId");
System.out.print("la categoria seleccionada es " +categoriaId);
String pagina = request.getParameter("pagina");
String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
ProductoDAO objProductoDAO = new ProductoDAO(objHibernate.getSessionFactory());


long cantidad_productos = objProductoDAO.contarProductosPorCategoria(categoriaId);
int cantidad_paginas = objProductoDAO.calcularCantidadPaginas(cantidad_productos, 5);

List<Producto> listaproducto = objProductoDAO.listarProductosPorCategoriaConLimite(categoriaId,Integer.parseInt(pagina),5);
response.setContentType("application/json;charset=UTF-8");
PrintWriter out = response.getWriter();out.print("{");
out.print("\"productos\": [");

for (int i = 0; i < listaproducto.size(); i++) {
    Producto producto = listaproducto.get(i);
    out.print("{");
    out.print("\"id\": \"" + producto.getProductoId() + "\",");
    out.print("\"nombre\": \"" + producto.getProductoNom() + "\",");
    out.print("\"precio\": \"" + producto.getProductoPrecio() + "\"");
    out.print("}");

    if (i < listaproducto.size() - 1) {
        out.print(",");
    }
}
out.print("],");
out.print("\"totalPaginas\": \"" + cantidad_paginas + "\",");
out.print("\"paginaActual\": \"" + pagina + "\",");
out.print("\"pagina\": \"" + pagina + "\"");
out.print("}");
System.out.print("la cantidad de paginas es " + cantidad_paginas);
objHibernate.closeSessionFactory();
}



}
