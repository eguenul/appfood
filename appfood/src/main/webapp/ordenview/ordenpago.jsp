<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario de B�squeda de �rdenes</title>
    
    <script src="scripts/ajax.js"></script>
</head>
<body>

 <h2>Formulario de B�squeda de �rdenes</h2>

 <label for="ordenNum">Nro Orden:</label>
 <input type="text" id="ordenNum" name="ordenNum">
 <button onclick="buscarOrdenes()">Buscar</button>
    <label for="paymentStatus">Estado de Pago:</label>
    <select id="paymentStatus" name="paymentStatus">
        <option value="">Todos</option>
        <option value="pagada">Pagada</option>
        <option value="pendiente">Pendiente</option>
        <!-- Agrega m�s opciones seg�n necesites -->
    </select>

    <button onclick="buscarOrdenes()">Buscar</button>

    <h3>Resultados de la B�squeda:</h3>
    <ul id="orderList">
        <!-- Los resultados se mostrar�n aqu� -->
    </ul>

    <script>
        function buscarOrdenes() {
         
        var ordenNum = document.getElementById("ordenNum").value;
              cargarAjax('BuscaOrden','ordenNum='+ordenNum,'orderList');
        }

        // Puedes agregar m�s funciones seg�n sea necesario
    </script>

</body>
</html>

