<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%
    // Obtener la fecha actual del servidor
    Date fechaActual = new Date();
    // Formatear la fecha en el formato que necesitas
    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
    String fechaFormateada = formatoFecha.format(fechaActual);
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Libro de Ventas por Fecha</title>
    <!-- Agrega los enlaces a los archivos CSS de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<%@include file="../include/nav.jsp" %>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h2 class="mb-4">Libro de Ventas por Forma de Pago</h2>
            
            <!-- Formulario -->
            <form action="LibroVenta" method="POST">
                <div class="mb-3">
                    <label for="fechaInicio" class="form-label">Fecha de Inicio:</label>
                    <input type="date" value="<% out.print(fechaFormateada); %>" class="form-control" id="fechaInicio" name="fechaInicio" required>
                </div>
                
                <div class="mb-3">
                    <label for="fechaFin" class="form-label">Fecha de Fin:</label>
                    <input type="date" value="<% out.print(fechaFormateada); %>" class="form-control" id="fechaFin" name="fechaFin" required>
                </div>
                
                
                    
                <div class="mb-3">
                    <label for="FPago" class="form-label">Forma de Pago:</label>
                    <select name="FPago" id="FPago">
                        <option value="PDF">PDF</option>                      
                        <option value="XLS">XLS</option>
                    </select>  
                </div>
                
                
                
                
                <div class="mb-3">
                    <label for="Formato" class="form-label">Formato:</label>
                    <select name="Formato" id="Formato">
                        <option value="PDF">PDF</option>                      
                        <option value="XLS">XLS</option>
                    </select>  
                </div>
                
                
                
                
                
                
                <button type="submit" class="btn btn-primary">Generar Reporte</button>
                
                
            </form>
            <!-- Fin del formulario -->
            
        </div>
    </div>
</div>

<!-- Agrega los enlaces a los archivos JS de Bootstrap y Popper.js (necesario para algunos componentes de Bootstrap) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</body>
</html>
