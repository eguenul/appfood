/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.productos;

import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

/**
 *
 * @author esteban
 */

public class ProductoDAO {

    private final SessionFactory sessionFactory;

    public ProductoDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
   public List<Producto> buscarPorNombre(String nombre) {
    try (Session session = sessionFactory.openSession()) {
        return session.createQuery("FROM Producto WHERE ProductoNom LIKE :nombre", Producto.class)
                .setParameter("nombre", "%" + nombre + "%")
                .list();
    } catch (Exception e) {
        return null;
    }
}
    
public Producto buscarPorId(int productoId) {
    try (Session session = sessionFactory.openSession()) {
        return session.get(Producto.class, productoId);
    } catch (Exception e) {
        return null;
    }
}    
    
    
    

    public void agregarProducto(Producto producto) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(producto);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public Producto obtenerProducto(int productoId) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Producto.class, productoId);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Producto> obtenerTodosProducto() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Producto", Producto.class).list();
        } catch (Exception e) {
            return null;
        }
    }

    public void actualizarProducto(Producto producto) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(producto);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void eliminarProducto(int productoId) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            Producto producto = session.get(Producto.class, productoId);
            if (producto != null) {
                session.delete(producto);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
    
    public List<Producto> listarProductosConLimite() {
        try (Session session = sessionFactory.openSession()) {
            // Hacer una consulta para obtener los primeros 5 productos
            String hql = "FROM Producto";
            Query<Producto> query = session.createQuery(hql, Producto.class);
            query.setMaxResults(5);
            List<Producto> productos = query.list();
            return productos;
        } catch (Exception e) {
// Manejar excepciones según tu lógica de aplicación
                        return null;
        }
    }
    
    
public List<Producto> listarProductosPorCategoriaConLimite(String categoriaId, int pagina, int productosPorPagina) {
    try (Session session = sessionFactory.openSession()) {
        // Hacer una consulta para obtener los productos de una categoría con paginación
        String hql = "FROM Producto WHERE CategoriaId = :categoriaId";
        Query<Producto> query = session.createQuery(hql, Producto.class);
        query.setParameter("categoriaId", categoriaId);

        // Calcula los índices de inicio y fin para la paginación
        int startIndex = (pagina - 1) * productosPorPagina;
        query.setFirstResult(startIndex);
        query.setMaxResults(productosPorPagina);

        List<Producto> productos = query.list();
        return productos;
    } catch (Exception e) {
        // Manejar excepciones según tu lógica de aplicación
        return null;
    }
}
   
public long contarProductosPorCategoria(String categoriaId) {
    try (Session session = sessionFactory.openSession()) {
        String sql = "SELECT COUNT(*) as Cantidad FROM Producto where CategoriaId=:categoriaId";
        Query query = session.createNativeQuery(sql);
        query.setParameter("categoriaId", Integer.valueOf(categoriaId));
        BigInteger result = (BigInteger) query.getSingleResult();
                
System.out.print("la cantidad de productos es: " + result.longValue());       
return result.longValue();
    } catch (Exception e) {
        // Manejar excepciones según tu lógica de aplicación
        return 0;
    }
}

public int calcularCantidadPaginas(long totalProductos, int productosPorPagina) {
    System.out.print(totalProductos);
    // Calcular la cantidad de páginas redondeando hacia arriba
    return (int) Math.ceil((double) totalProductos / productosPorPagina);
}

public List<Producto> listarProductosPorCategoriaYPagina(String categoriaId, int pagina, int productosPorPagina) {
    try (Session session = sessionFactory.openSession()) {
        // Calcular los índices de inicio y fin para la paginación
        int startIndex = (pagina - 1) * productosPorPagina;

        // Crear la consulta SQL nativa
        String sql = "SELECT * FROM Producto WHERE CategoriaId = :categoriaId LIMIT :startIndex, :productosPorPagina";

        // Crear la consulta y asignar parámetros
        Query<Producto> query = session.createNativeQuery(sql, Producto.class);
        query.setParameter("categoriaId", categoriaId);
        query.setParameter("startIndex", startIndex);
        query.setParameter("productosPorPagina", productosPorPagina);

        // Ejecutar la consulta y obtener la lista de productos
        List<Producto> productos = query.getResultList();

        return productos;
    } catch (Exception e) {
        // Manejar excepciones según tu lógica de aplicación
        return null;
    }
}    

}
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    

