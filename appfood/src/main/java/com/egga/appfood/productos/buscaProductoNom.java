/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.productos;

import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */


@WebServlet(urlPatterns = "/buscaProductoNom", name = "buscaProductoNom")
public class buscaProductoNom extends HttpServlet {
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {

 String productoNom = request.getParameter("ProductoNom");
 String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
 HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
 ProductoDAO objProductoDAO = new ProductoDAO(objHibernate.getSessionFactory());
 List<Producto> listaproducto = objProductoDAO.buscarPorNombre(productoNom);
response.setContentType("text/html;charset=UTF-8"); 
PrintWriter out = response.getWriter();
for(Producto producto:listaproducto){ 
    out.print("<tr>");
    out.print("<td>"+producto.getProductoId()+"</td>");
    out.print("<td>"+producto.getProductoNom()+"</td>");
    out.print("<td>"+producto.getProductoPrecio()+"</td>");
    out.print("</tr>");
}
  objHibernate.closeSessionFactory();  
} 
    
}
