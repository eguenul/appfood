/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.fpago;

/**
 *
 * @author esteban
 */
public class FPago {
    
    private int fpagoid;
    private String fpagodes;

    public int getFpagoid() {
        return fpagoid;
    }

    public void setFpagoid(int fpagoid) {
        this.fpagoid = fpagoid;
    }

    public String getFpagodes() {
        return fpagodes;
    }

    public void setFpagodes(String fpagodes) {
        this.fpagodes = fpagodes;
    }
    
}
