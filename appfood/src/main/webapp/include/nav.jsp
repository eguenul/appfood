<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <!-- Marca y bot�n para colapsar en pantallas peque�as -->
        <a class="navbar-brand" href="#">AppFood</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Contenido de la barra de navegaci�n -->
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">Inicio</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Mantenci�n
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="mainCategoria">Categor&iacute;as</a>
                        <a class="dropdown-item" href=mainProducto>Productos</a>
                        <a class="dropdown-item" href="#">Mesas</a>
                    </div>
                </li>
                <li class="nav-item dropdown ">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       Ordenes de Pedido
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">           
                       <a class="dropdown-item" href="mainOrden">Emision Orden de Pedido</a>
                       <a class="dropdown-item" href="PagoOrden">Pago Orden</a>
                    </div>
                    
                   </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       Informes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                           <a class="dropdown-item" href="#">Historial de Ordenes</a>
                        <a class="dropdown-item" href="#">Libro de Ventas</a>
                        <a class="dropdown-item" href="#">Libro de Ventas por Categor&iacute;as</a>
                        <a class="dropdown-item" href="#">Libro de Ventas por Producto</a>
                        <a class="dropdown-item" href="#">Libro de Ventas por Forma de Pago</a>
                    </div>
                </li>
                
                
                
                
                <!-- Agrega m�s elementos de men� seg�n sea necesario -->
            </ul>
        </div>
    </div>
</nav>

