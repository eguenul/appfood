
package com.egga.appfood.include;

import java.io.File;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
     private final SessionFactory sessionFactory;
    
    
    public HibernateUtil(String rutaHBConfig){
       
        this.sessionFactory = new Configuration().configure(new File(rutaHBConfig))
                    .buildSessionFactory();
    }
    
    public  SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }
    
      public void closeSessionFactory() {
        this.sessionFactory.close();
    }
    
}
