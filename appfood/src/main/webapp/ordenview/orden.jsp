<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%
    // Obtener la fecha actual del servidor
    Date fechaActual = new Date();
    // Formatear la fecha en el formato que necesitas
    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
    String fechaFormateada = formatoFecha.format(fechaActual);
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Orden Pedido </title>
    <!-- Agrega los enlaces a los archivos CSS de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="scripts/Producto.js"></script>
     <script src="scripts/ajax.js"></script>
     <script src="scripts/Orden.js"></script>
    </head>
<body>


<%@include file="../include/nav.jsp" %>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-6">
            <h2 class="mb-4">Orden de Pedido</h2>
            
            <!-- Formulario -->
             <div class="mb-3">
                    <label for="auxFechaOrden" class="form-label">Fecha Orden:</label>
                    <input value="<% out.print(fechaFormateada); %>" type="date" class="form-control" id="auxFechaOrden" name="auxFechaOrden" required>
                </div>
                    
            
            
            <!--
            <div class="mb-3">
                    <label for="nombreCliente" class="form-label">Nombre del Cliente:</label>
                    <input type="text" class="form-control" id="nombreCliente" name="nombreCliente" required>
                </div>
                -->
                <!-- Ejemplo de control select -->
                <div class="mb-3">
                    <label for="auxMesaId" class="form-label">Mesa:</label>
                    <select class="form-select" id="auxMesaId" aria-label="Selecciona una opci�n">
                       <option value="opcion1">Mesa 1</option>
                        <option value="opcion2">Mesa 2</option>
                        <option value="opcion3">Mesa 3</option>
                    </select>
                </div>
                            <div class="mb-3">
                    <label for="producto" class="form-label">Producto:</label>
                    <input type="text" readonly="yes" class="form-control" id="producto" name="producto" required>
                </div>

                <div class="mb-3">
                    <label for="cantidad" class="form-label">Cantidad:</label>
                    <input type="number" oninput="calcularTotal();" class="form-control" id="cantidad" name="cantidad" required>
                </div>

            <div class="mb-3">
                    <label for="precio" class="form-label">Precio:</label>
                    <input type="number" readonly="yes" class="form-control" id="precio" name="precio" required>
                </div>
            
            <div class="mb-3">
                    <label for="total" class="form-label">Total:</label>
                    <input type="number" readonly="yes"  class="form-control" id="total" name="total" required>
                </div>
            
            
                <div class="mb-3">
                    <label for="observaciones" class="form-label">Observaciones:</label>
                    <textarea class="form-control" id="observaciones" name="observaciones" rows="3"></textarea>
                </div>

                <button type="button" onclick="agregaDetalle();" class="btn btn-primary">Agregar a la Comanda</button>
                <!-- Botn para abrir el modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalProductos">
                   Ver Productos
                </button>
                <button type="button" class="btn btn-primary" onclick="grabarOrden();" >
                Grabar</button>
                <input type="hidden" id="auxProductoId" name="auxProductoId">                
           
        </div>
        
        <div class="col-md-6">
            <!-- Tabla de Comanda -->
            <div class="mt-4">
                <h4>Detalle de Orden de Pedido</h4>
               <form name="formComanda" method="POST" action="addOrden">
        
                <table id="TablaDetalle" class="table">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Total</th>
                            <th>Observaciones</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Aqu se mostrarn dinmicamente los elementos aadidos a la comanda -->
                    </tbody>
                    <tr>
                    <td colspan="3"></td>
                    <td>Total Orden:</td>
                    <td><input readonly="yes" id="totalOrden" name="totalOrden"></td>
                    <td></td>
                </tr>
                </table>
                       <input type="hidden" id="mesaComanda" name="mesaComanda">
                   <input type="hidden" value="0" id="NRO_FILAS" name="NRO_FILAS">
                   <input type="hidden" id="OrdenFecha" name="OrdenFecha">
                   
                   </form>
            <!-- Fin del formulario --> 
            </div>
            <!-- Fin de la Tabla de Comanda -->
        </div>
    </div>
</div>

<%@include file="../productoview/modalproducto.jsp" %>
<!-- Agrega los enlaces a los archivos JS de Bootstrap y Popper.js (necesario para algunos componentes de Bootstrap) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</body>
</html>
