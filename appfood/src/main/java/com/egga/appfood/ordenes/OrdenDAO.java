package com.egga.appfood.ordenes;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

public class OrdenDAO {

    private final SessionFactory sessionFactory;

    public OrdenDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void guardarOrden(Orden orden) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(orden);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

public Orden buscarPorNum(int ordenNum) {
    try (Session session = sessionFactory.openSession()) {
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            // Ejecuta una consulta SQL nativa con una cláusula WHERE
            SQLQuery query = session.createSQLQuery("SELECT * FROM Orden WHERE OrdenNum = :ordenNum");
            query.setParameter("ordenNum", ordenNum);
            query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
            List result = query.list();

            tx.commit();

            for (Object row : result) {
                Map rowMap = (Map) row;
                // Crea la instancia dentro del bucle
                Orden objOrden = new Orden();
                // Accede a las columnas por nombre
                objOrden.setOrdenId((int) rowMap.get("OrdenId"));
                objOrden.setOrdenNum((int) rowMap.get("OrdenNum"));
                objOrden.setOrdenTotal((int) rowMap.get("OrdenTotal"));
                objOrden.setOrdenFecha((Date) rowMap.get("OrdenFecha"));

                // Haz algo con los datos obtenidos

                return objOrden; // Devuelve la orden encontrada
            }
        } catch (HibernateException e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
        }
    } catch (Exception e) {
    }

    return null; // Devuelve null si no se encuentra ninguna orden
}


    

    public int getId(int ordenNum) {
        try (Session session = sessionFactory.openSession()) {
            // Utiliza consultas de Hibernate para mantener la coherencia
            Query<Integer> query = session.createQuery("SELECT o.ordenId FROM Orden o WHERE o.ordenNum = :ordenNum", Integer.class);
            query.setParameter("ordenNum", ordenNum);
            return query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            return 0; // O devuelve un valor que indique error, según tus necesidades
        }
    }

      public void actualizarOrden(int ordenNum, int NumVoucher, int fpagoid) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            try {
                NativeQuery<?> query = session.createNativeQuery("Update Orden set NumVoucher=" + String.valueOf(NumVoucher)+", FPagoId="+String.valueOf(fpagoid) +", OrdenPago=1 Where OrdenNum="+ String.valueOf(ordenNum)   );
                query.executeUpdate();
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                throw e;
            }
        }
    }
    
    
    
    
    
    
    // Otros métodos del DAO según tus necesidades (actualizar, eliminar, buscar, etc.)
}
