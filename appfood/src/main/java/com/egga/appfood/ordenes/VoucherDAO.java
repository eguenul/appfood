package com.egga.appfood.ordenes;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;

/**
 * Clase que representa el acceso a datos para la entidad Voucher.
 */
public class VoucherDAO {
    private final SessionFactory sessionFactory;

    public VoucherDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Método para buscar un voucher por su número.
     *
     * @param numVoucher Número del voucher a buscar.
     * @return Array con los datos del voucher o un array vacío si no se encuentra.
     */
    public Object[] buscaVoucher(int numVoucher) {
        Object[] arrayVoucher = new Object[8];

        try (Session session = sessionFactory.openSession()) {
            String sqlQuery = "SELECT Orden.OrdenId, Orden.OrdenNum, Orden.OrdenPago, Orden.OrdenFecha, Orden.OrdenTotal, Orden.FPagoId, FPago.FPagoDes, Orden.NumVoucher FROM Orden " +
                              "INNER JOIN FPago ON Orden.FPagoId = FPago.FPagoId " +
                              "WHERE Orden.NumVoucher = :numVoucher";

            NativeQuery<Object[]> query = session.createNativeQuery(sqlQuery);
            query.setParameter("numVoucher", numVoucher);

            Object[] result = query.uniqueResult();

            // Asignar los valores a arrayVoucher
            
            // Asegurarse de que las posiciones del array coincidan con las columnas en la consulta
            arrayVoucher[0] = result[0]; // OrdenId
            arrayVoucher[1] = result[1]; // OrdenNum
            arrayVoucher[2] = result[2]; // OrdenPago
            arrayVoucher[3] = result[3]; // OrdenFecha
            arrayVoucher[4] = result[4]; // OrdenTotal
            arrayVoucher[5] = result[5]; // FPagoId
            arrayVoucher[6] = result[6]; // FPagoDes
            arrayVoucher[7] = result[7]; // NumVoucher
        }

        return arrayVoucher;
    }
}