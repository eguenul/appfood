/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.productos;

import com.egga.appfood.categoria.Categoria;

/**
 *
 * @author esteban
 */
public class Producto {
private int ProductoId;
private String ProductoNom;
private int ProductoPrecio;
private Categoria categoria;
    public int getProductoId() {
        return ProductoId;
    }

    public void setProductoId(int ProductoId) {
        this.ProductoId = ProductoId;
    }

    public String getProductoNom() {
        return ProductoNom;
    }

    public void setProductoNom(String ProductoNom) {
        this.ProductoNom = ProductoNom;
    }

    public int getProductoPrecio() {
        return ProductoPrecio;
    }

    public void setProductoPrecio(int ProductoPrecio) {
        this.ProductoPrecio = ProductoPrecio;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }


}
