<%@page import="com.egga.appfood.ordenes.Orden"%>
<%@page import="java.util.List"%>
<!-- Dentro del body de tu p�gina -->
<h3>Resultados de la B�squeda:</h3>

<form name="formProcesaPago" method="POST" action="ProcesaPago">
<table id="orderTable">
    <thead>
        <tr>
            <th>N�mero de Orden</th>
            <th>Fecha Orden</th>
            <th>Total Orden</th>
            <th>Estado de Pago</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <!-- Ejemplo de fila de resultado -->
        
         <% List<Orden> listaOrden = (List<Orden>)request.getSession(true).getAttribute("arrayOrdenList");
            %>               
             
        <%   for(Orden orden : listaOrden){ %>
        <tr>
            <td><% out.print(orden.getOrdenNum()); %></td>
            <td><% out.print(orden.getOrdenFecha()); %></td>
            <td><% out.print(orden.getOrdenTotal()); %></td>
            <td>Pendiente</td>
            <td>
                <button onclick="document.formProcesaPago.ordenNum2.value='<% out.print(orden.getOrdenNum()); %>'; document.formProcesaPago.submit();">Marcar como Pagada</button>
                <!-- Puedes agregar m�s botones seg�n necesites -->
        </td>
        </tr>
        <% } %>
</tbody>
</table>
<input type="hidden" name="ordenNum2">
</form>