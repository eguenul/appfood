/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.categoria;

import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */
@WebServlet(urlPatterns = "/addCategoria", name = "addCategoria")
public class addCategoria extends HttpServlet {
        
   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
      String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
      HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
      CategoriaDAO objCategoriaDAO = new CategoriaDAO(objHibernate.getSessionFactory());
    
      Categoria objCategoria = new Categoria();
      objCategoria.setCategoriaDes(request.getParameter("CategoriaDes"));
      objCategoriaDAO.agregarCategoria(objCategoria);
      objHibernate.closeSessionFactory();
      response.sendRedirect("messages/categoria.html");
      
      
  
    }
    
    
    
    
}
