/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.productos;


import com.egga.appfood.categoria.Categoria;
import com.egga.appfood.categoria.CategoriaDAO;
import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */


@WebServlet(urlPatterns = "/mainProducto", name = "mainProducto")
public class mainProducto extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
    throws IOException, ServletException {
     
    String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
    HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
    CategoriaDAO objCategoriaDAO = new CategoriaDAO(objHibernate.getSessionFactory());
    List<Categoria> arrayCategoria = objCategoriaDAO.obtenerTodasCategorias();
    request.getSession().setAttribute("arrayCategoria", arrayCategoria);
    objHibernate.closeSessionFactory();
    getServletConfig().getServletContext().getRequestDispatcher("/productoview/addproducto.jsp").forward(request,response);
   
  
    }
}


