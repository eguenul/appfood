/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.ordenes;

/**
 *
 * @author esteban
 */
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class DetalleOrdenDAO {
    
      private final SessionFactory sessionFactory;

    
     public DetalleOrdenDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void addDetalle(DetalleOrden detalle_orden) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(detalle_orden);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
    
    public List<Object[]> obtenerDetallesDeOrden(int ordenId) {
        List<Object[]> results = null;
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            // Inicia la transacción
            tx = session.beginTransaction();

            String sqlQuery = "SELECT Producto.ProductoNom, DetalleOrden.Cantidad, DetalleOrden.Precio, DetalleOrden.Total, DetalleOrden.Observacion \n" +
                    "FROM DetalleOrden \n" +
                    "INNER JOIN Producto ON DetalleOrden.ProductoId = Producto.ProductoId \n" +
                    "WHERE DetalleOrden.OrdenId = :ordenId";

            results = session.createSQLQuery(sqlQuery)
                    .setParameter("ordenId", ordenId)
                    .list();

            // Commit de la transacción
            tx.commit();
        } catch (Exception e) {
            // Maneja la excepción, hace un rollback y/o realiza cualquier otra acción necesaria
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            // Cambia esto por un manejo adecuado de las excepciones
        }
        return results;
    }
    
}
