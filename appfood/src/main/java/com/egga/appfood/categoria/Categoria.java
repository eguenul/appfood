/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.categoria;

/**
 *
 * @author esteban
 */
public class Categoria {
    private int CategoriaId;
    private String CategoriaDes;

    public int getCategoriaId() {
        return CategoriaId;
    }

    public void setCategoriaId(int CategoriaId) {
        this.CategoriaId = CategoriaId;
    }

    public String getCategoriaDes() {
        return CategoriaDes;
    }

    public void setCategoriaDes(String CategoriaDes) {
        this.CategoriaDes = CategoriaDes;
    }
            
}
