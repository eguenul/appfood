/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.reportes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */

@WebServlet(urlPatterns = "/ShowLibro1", name = "ShowLibro1")
public class ShowLibro1 extends HttpServlet {
    
    
      @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

         File file = new File("/home/esteban/Venta.pdf");
        response.setHeader("Content-Type", getServletContext().getMimeType(file.getName().trim()));
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "inline; filename=\""+"Libro.pdf" + "\"");
        Files.copy(file.toPath(), response.getOutputStream());
        file.delete();
       // Ajusta la ruta al directorio donde se genera el PDF por el servlet PrintOrden
      
}
    
    
    
    
    
}
