/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.productos;

import com.egga.appfood.categoria.Categoria;
import com.egga.appfood.categoria.CategoriaDAO;
import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */


@WebServlet(urlPatterns = "/addProducto", name = "addProducto")
public class addProducto extends HttpServlet {

@Override
public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
   String rutaRelativaApp= getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
   HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
   CategoriaDAO objCategoriaDAO = new CategoriaDAO(objHibernate.getSessionFactory());
  
   int categoriaId = Integer.parseInt(request.getParameter("CategoriaId"));
  
   Categoria objCategoria = objCategoriaDAO.obtenerCategoria(categoriaId);
   Producto objProducto = new Producto();
   objProducto.setCategoria(objCategoria);
   objProducto.setProductoNom(request.getParameter("ProductoNom"));
   objProducto.setProductoPrecio(Integer.parseInt(request.getParameter("ProductoPrecio")));
   
   ProductoDAO objProductoDAO = new ProductoDAO(objHibernate.getSessionFactory());
   objProductoDAO.agregarProducto(objProducto);
   objHibernate.closeSessionFactory();
   request.getSession().setAttribute("mensajeOK", "OK");
   response.sendRedirect("mainProducto");
   
}
    
}
