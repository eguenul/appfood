/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.fpago;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;






/*
 * @author esteban
 */
public class FPagoDAO {
     private final SessionFactory sessionFactory;
    
    
    public FPagoDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    

    public List<FPago> listarFormaPago() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM FPago", FPago.class).list();
        } catch (Exception e) {
            return null;
        }
    }
}
