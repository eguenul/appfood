<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Formulario de Categor�a</title>
    <!-- Agrega los enlaces a los archivos CSS de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@include file="../include/nav.jsp" %>
<div class="container mt-5">
    <h2 class="mb-4">Formulario de Categor�a</h2>
    
    <!-- Formulario -->
    <form method="POST" action="addCategoria">
        <div class="mb-3">
            <label for="CategoriaId" class="form-label">ID de Categor�a:</label>
            <input type="text" readonly class="form-control" id="CategoriaId" name="CategoriaId">
           
        </div>
        
        <div class="mb-3">
            <label for="CategoriaDes" class="form-label">Descripci�n de Categor�a:</label>
            <input type="text" class="form-control" id="CategoriaDes" name="CategoriaDes" required>
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#modalCategorias">
            <i class="bi bi-search"></i> Buscar
        </button>
        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#modalCategorias">
            <i class="bi bi-search"></i> Salir
        </button>
    </form>
    <!-- Fin del formulario -->

</div>

<!-- Agrega los enlaces a los archivos JS de Bootstrap y Popper.js (necesario para algunos componentes de Bootstrap) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>