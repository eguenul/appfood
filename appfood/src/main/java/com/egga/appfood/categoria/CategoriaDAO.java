/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.categoria;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import java.util.List;

public class CategoriaDAO {

    private final SessionFactory sessionFactory;

    public CategoriaDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void agregarCategoria(Categoria categoria) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(categoria);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public Categoria obtenerCategoria(int categoriaId) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Categoria.class, categoriaId);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Categoria> obtenerTodasCategorias() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Categoria", Categoria.class).list();
        } catch (Exception e) {
            return null;
        }
    }

    public void actualizarCategoria(Categoria categoria) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(categoria);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void eliminarCategoria(int categoriaId) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            Categoria categoria = session.get(Categoria.class, categoriaId);
            if (categoria != null) {
                session.delete(categoria);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
