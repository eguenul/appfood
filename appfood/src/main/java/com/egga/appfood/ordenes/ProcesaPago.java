/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.ordenes;

import com.egga.appfood.fpago.FPago;
import com.egga.appfood.fpago.FPagoDAO;
import com.egga.appfood.include.HibernateUtil;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */



@WebServlet(urlPatterns = "/ProcesaPago", name = "ProcesaPago")
public class ProcesaPago extends HttpServlet {
       private float centimetrosAPuntos(float centimetros) {
        final float PUNTOS_POR_CENTIMETRO = 28.3465f;
        return centimetros * PUNTOS_POR_CENTIMETRO;
    }

    
    
    
       @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            String rutaRelativaApp = getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
            HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
            OrdenDAO objOrdenDAO = new OrdenDAO(objHibernate.getSessionFactory());
                 
          
            int ordenNum =Integer.parseInt(request.getParameter("ordenNum2"));
            
            
              
            Orden objOrden = objOrdenDAO.buscarPorNum(ordenNum);
            
             if (objOrden != null) {
            System.out.println("Orden encontrada: " + objOrden);
        } else {
            System.out.println("Orden no encontrada para el número: " + ordenNum);
        }
            
            

            /* cargo el template pdf */
            PdfReader reader = new PdfReader(getServletContext().getRealPath("/template/template.pdf"));
       /*
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream("/home/esteban/orden.pdf"));
         */   
           
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream("/home/esteban/appfood/PDF/orden.pdf")); 
            
            PdfContentByte content = stamper.getOverContent(1);
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.EMBEDDED);
            content.setFontAndSize(bf, 55);

            /* ahora imprimo el rut y el folio del recuadro */
           
            float coordenadaXCentimetros = 20.0f;
            float coordenadaYCentimetros = 104.0f;

            float coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
            float coordenadaYPuntos = centimetrosAPuntos(coordenadaYCentimetros);

            content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
            content.showText(String.valueOf(ordenNum));
           
            
            
            
             coordenadaXCentimetros = 13.0f;
             coordenadaYCentimetros = 99.9f;

             coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
             coordenadaYPuntos = centimetrosAPuntos(coordenadaYCentimetros);

            
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

        // Formatea la fecha según el formato
            content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
            content.showText(formatoFecha.format(objOrden.getOrdenFecha()));
            
              DetalleOrdenDAO objDetalleDAO = new DetalleOrdenDAO(objHibernate.getSessionFactory());
       
            List<Object[]> results = objDetalleDAO.obtenerDetallesDeOrden(objOrden.getOrdenId());
            
            float tamanoFuenteDetalles = 40;
             BaseFont bfDetalles = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
 content.setFontAndSize(bfDetalles, tamanoFuenteDetalles);
  
    coordenadaYCentimetros =  coordenadaYCentimetros - 15.0f;  // Espacio entre cada línea de detalle
    coordenadaYPuntos = centimetrosAPuntos(coordenadaYCentimetros);
  
            for (Object[] detalle : results) {
                String productoNom = (String) detalle[0];
                int cantidad = (int) detalle[1];
                int precio = (int) detalle[2];
                int total = (int) detalle[3];
                String observacion = (String) detalle[4];
                // Haz algo con la información, como imprimir o almacenar en una estructura de datos.
    
   
    coordenadaXCentimetros = 2.5f;
    coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
                       
    
    
    
    content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
    content.showText(productoNom);
    
  
    coordenadaXCentimetros = 25.0f;
    coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
                       
    content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
    content.showText(String.valueOf(cantidad));
    
  
    coordenadaXCentimetros = 35.0f;
    coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
                       
    content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
    content.showText(String.valueOf(precio));
    
   
    coordenadaXCentimetros = 47.0f;
    coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
                       
    content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
     content.showText(String.valueOf(total));
    
   coordenadaYCentimetros =  coordenadaYCentimetros - 1.5f;  // Espacio entre cada línea de detalle
   coordenadaYPuntos = centimetrosAPuntos(coordenadaYCentimetros);
   
   coordenadaXCentimetros = 2.5f;
   coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
   content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
   content.showText(observacion);
    
    
    
   coordenadaYCentimetros =  coordenadaYCentimetros - 1.5f;  // Espacio entre cada línea de detalle
   coordenadaYPuntos = centimetrosAPuntos(coordenadaYCentimetros);
         }
            
            
    coordenadaXCentimetros = 35.0f;
    coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
                     
    content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
    content.setFontAndSize(bf, 40);
    
    content.showText("TOTAL ORDEN:" );
            
            
    coordenadaXCentimetros = 47.0f;
    coordenadaXPuntos = centimetrosAPuntos(coordenadaXCentimetros);
    content.setTextMatrix(coordenadaXPuntos, coordenadaYPuntos);
    content.showText(String.valueOf(objOrden.getOrdenTotal()));
    stamper.close();
   
    FPagoDAO  objFPagoDAO =  new FPagoDAO(objHibernate.getSessionFactory());
    List<FPago> arrayFPago  =  objFPagoDAO.listarFormaPago();
   
    request.getSession().setAttribute("arrayFPago",arrayFPago);
    request.getSession().setAttribute("ordenNum", ordenNum);
    
    
    
   getServletConfig().getServletContext().getRequestDispatcher("/ordenview/procesapago.jsp").forward(request,response);
  
        } catch (IOException | DocumentException ex) {
            Logger.getLogger(PrintOrden.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
