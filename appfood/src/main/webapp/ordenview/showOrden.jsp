<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Visualizar PDF</title>
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <%@include file="../include/nav.jsp" %>
    
    
    <h1>ORDEN DE PEDIDO</h1>
      <button onclick="goBack()">Volver</button>
    <embed src="DownloadOrden" type="application/pdf" width="100%" height="600px" />
    <br>
  

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
<!-- Agrega los enlaces a los archivos JS de Bootstrap y Popper.js (necesario para algunos componentes de Bootstrap) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</body>
</html>