/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appfood.ordenes;
import com.egga.appfood.correlativo.CorrelativoDAO;
import com.egga.appfood.include.HibernateUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/ProcesaVoucher", name = "ProcesaVoucher")
public class ProcesaVoucher extends HttpServlet {
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

  int ordenNum = Integer.parseInt(request.getParameter("ordenNum"));
  int fpagoid = Integer.parseInt(request.getParameter("FPagoId"));
  String rutaRelativaApp = getServletContext().getRealPath("/WEB-INF/hibernate1.cfg.xml");
  HibernateUtil objHibernate = new HibernateUtil(rutaRelativaApp);
 OrdenDAO objOrdenDAO = new OrdenDAO(objHibernate.getSessionFactory());
 CorrelativoDAO objCorrelativo = new CorrelativoDAO(objHibernate.getSessionFactory()); 
 int NumVoucher = objCorrelativo.obtenerCorrelativoVoucher();
 objCorrelativo.actualizarVoucher();
 objOrdenDAO.actualizarOrden(ordenNum,NumVoucher,fpagoid);
 request.getSession().setAttribute("NumVoucher", NumVoucher);
 response.sendRedirect("ImprimeVoucher");
    
}    
    
    
}
