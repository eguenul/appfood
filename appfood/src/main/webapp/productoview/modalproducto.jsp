<%@page import="com.egga.appfood.categoria.Categoria"%>
<%@page import="java.util.List"%>
<%@page import="com.egga.appfood.productos.Producto"%>
<!-- Modal para mostrar los productos -->
<div class="modal fade" id="modalProductos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Lista de Productos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <!-- Campo de b�squeda por texto -->
        <div class="mb-3">
          <label for="ProductoNom2" class="form-label">Buscar por Texto:</label>
          <input type="text" class="form-control" id="ProductoNom2" oninput="buscaProductoNom();">
        </div>

        <!-- Campo de b�squeda por ID -->
        <div class="mb-3">
          <label for="campoBusquedaID" class="form-label">Buscar por ID:</label>
          <input type="text" class="form-control" id="campoBusquedaID" oninput="filtrarProductos()">
        </div>

        <!-- Men� desplegable de categor�as -->
        <div class="mb-3">
          <label for="selectCategoria" class="form-label">Seleccionar Categor�a:</label>
          <select class="form-select" id="selectCategoria" onchange="filtrarProductos()">
            <option selected value="0">Todas las Categor�as</option>
              <% List<Categoria> listCategoria = (List<Categoria>)request.getSession(true).getAttribute("arrayCategoria");
            %>               
                
            <%   for(Categoria categoria : listCategoria){ %>
             <option value="<% out.print(categoria.getCategoriaId()); %>"><% out.print(categoria.getCategoriaDes()); %></option>
            <% } %>
            <!-- Agrega m�s opciones seg�n tus categor�as -->
          </select>
        </div>
    
         <!-- Aqu� se mostrar� la tabla de productos -->
        <div id="contenidoProductos">
          <table class="table">
            <thead>
                                           <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Accion</th>
              </tr>
           
            
            </thead>
            <tbody id="tablaProductosBody">
            <!-- Aqu� se agregar�n las filas de productos din�micamente -->
            <%
                List<Producto> listaproducto = (List<Producto>) request.getSession().getAttribute("listaproducto");
               for(Producto producto:listaproducto){
            %>
            <tr>
                <td><% out.print(producto.getProductoId()); %></td>
                <td><% out.print(producto.getProductoNom()); %></td>
                <td><% out.print(producto.getProductoPrecio()); %></td>
                <td><button type="button" onclick="seleccionaProducto('<% out.print(producto.getProductoId()); %>')">Seleccionar</button></td>
            </tr>
             <% } %>
            </tbody>
          </table>
        </div>
            <!-- Controles de paginaci�n -->
<div class="mb-3">
  <button type="button" class="btn btn-primary" onclick="cambiarPagina('anterior')">Anterior</button>
  <span id="paginaActual">1</span>
  <button type="button" class="btn btn-primary" onclick="cambiarPagina('siguiente')">Siguiente</button>
  <input type="hidden" id="NRO_PAGINAS" value="">
  <input type="hidden"  id="pagina" name="pagina" value="1">
</div>

            
            
            
      </div>
        
        
        
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>